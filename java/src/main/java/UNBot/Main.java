package UNBot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

import com.google.gson.Gson;

public class Main 
{
	private static boolean debug;
    public static void main(String... args) throws IOException 
    {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];
        if (args.length > 4 && args[4].trim().toLowerCase().equals("debug"))
        {
        	debug = true;
        	System.out.println("Debug is ON");
        }
        else
        {
        	debug = false;
        	System.out.println("Debug is OFF");
        }

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    private YourCar myCar;
    private ArrayList<Track>gameTrack;
    private boolean firstLapDone;
    private GameInit gameInit;
    private double carThrottle;
    private boolean turboAvailable;
    private TurboAvailable turbo;
    private int tick;
    
    private CarPositions currentPositions;
    private CarPositions previousPositions;
    private CarPosition currentMyCarPosition;
    private CarPosition previousMyCarPosition;
    private boolean skipTurbo;
    private double actualSpeed;
    
    private boolean carCurrentlyInTurn;
    
    @SuppressWarnings("unused")
	public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException 
    {
        this.writer = writer;
        String line = null;

        send(join);

        // Creating new list for the track. It will be filled during the first lap
        firstLapDone = false;
        gameTrack = new ArrayList<Track>();
        carThrottle = 0.5;
        turboAvailable = false;
        skipTurbo = true;
        send(new Turbo());
        tick = 0;
        carCurrentlyInTurn = false;
        while((line = reader.readLine()) != null) 
        {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) 
            {
            	// Setting the car position data in the private variable
            	previousPositions = currentPositions;
            	currentPositions = gson.fromJson(line, CarPositions.class);
            	previousMyCarPosition = currentMyCarPosition;
            	currentMyCarPosition = GetCarPosition(currentPositions, myCar.data);
            	// Calculating our actual speed
            	if (previousMyCarPosition != null)
            	{
            		// If we are still on the same track part as in the earlier tick
            		if (previousMyCarPosition.piecePosition.pieceIndex == currentMyCarPosition.piecePosition.pieceIndex)
            		{
            			actualSpeed = currentMyCarPosition.piecePosition.inPieceDistance - previousMyCarPosition.piecePosition.inPieceDistance;
            		}
            		else // We have moved to another track part
            		{
            			actualSpeed = (gameInit.data.race.track.pieces.get(previousMyCarPosition.piecePosition.pieceIndex).length - previousMyCarPosition.piecePosition.inPieceDistance)
            						+ currentMyCarPosition.piecePosition.inPieceDistance;
            		}
            	}
            	else 
            	{
            		actualSpeed = 0;
            	}
            	
            	// Checking if we are in a turn right now
            	carCurrentlyInTurn = checkIfCarInTurn();
            	
            	if (currentMyCarPosition != null)
            	{
            		// Checking that there aren't too big changes in the car's angle. If delta angle is more than 2.5, then we are about to crash
            		if (previousMyCarPosition != null && Math.abs(currentMyCarPosition.angle - previousMyCarPosition.angle) > 2.5 && carCurrentlyInTurn)
            		{
            			// Slowing down
            			if (carThrottle > 0.1)
            			{
            				carThrottle -= 0.1;
            				send(new Throttle(carThrottle));
            			}
            			else
            			{
            				send(new Ping());
            			}
            		}

	            	// EMERGENCY, car's angle is too big
            		else if (Math.abs(currentMyCarPosition.angle) > 40)
	            	{
		            		carThrottle = 0.1;
		            		send(new Throttle(carThrottle));
	            	}
	            	else if (Math.abs(currentMyCarPosition.angle) > 30)
	            	{

		            		carThrottle = 0.2;
		            		send(new Throttle(carThrottle));


	            	}
	            	else if (Math.abs(currentMyCarPosition.angle) > 25)
	            	{
		            		carThrottle = 0.3;
		            		send(new Throttle(carThrottle));
	            	}
	            	// If this is a straight line, then full speed
	            	else if (carCurrentlyInTurn == false)
	            	{
	            		// If turbo is available and the next 5 pieces are straight, then we will turn turbo on
	            		boolean turnTurboOn = false;
	            		if (turboAvailable == true && skipTurbo == false)
	            		{
	            			turnTurboOn = true;
	            			for (int i = 0; i < 5; ++i)
	            			{
	            				// If the piece index will start from the beginning
	            				if (currentMyCarPosition.piecePosition.pieceIndex + i < gameInit.data.race.track.pieces.size())
	            				{
	            					if (gameInit.data.race.track.pieces.get(currentMyCarPosition.piecePosition.pieceIndex + i).angle != 0)
	            					{
	            						// There is a turn coming, can't put turbo on
	            						turnTurboOn = false;
	            						break;
	            					}
	            				}
	            				else // Next piece is found from the beginning of the list
	            				{
	            					if ((currentMyCarPosition.piecePosition.pieceIndex + i) - gameInit.data.race.track.pieces.size() < 0)
	            					{
	            						debug("Negative index: " + ((currentMyCarPosition.piecePosition.pieceIndex + i) - gameInit.data.race.track.pieces.size()));
	            					}
	            					if (gameInit.data.race.track.pieces.get(gameInit.data.race.track.pieces.size() - (currentMyCarPosition.piecePosition.pieceIndex + i)).angle != 0)
	            					{
	            						turnTurboOn = false;
	            						break;
	            					}
	            				}
	            			}
	            		}
	            		
	            		if (turnTurboOn == true && skipTurbo == false)
	            		{
	            			debug("Turbo was turned on");
	            			send(new Turbo());
	            			turboAvailable = false;
	            		}
	            		else
	            		{
	            			// Depending on how fast our actual speed is, we have to calculate when to start slowing down in case of a turn
	            			int piecesBeforeBrake = 0;
	            			if (actualSpeed > 15)
	            			{
	            				piecesBeforeBrake = 5;
	            			}
	            			else if (actualSpeed > 12.5)
	            			{
	            				piecesBeforeBrake = 4;
	            			}
	            			else if (actualSpeed > 10)
	            			{
	            				piecesBeforeBrake = 3;
	            			}
	            			else if (actualSpeed > 7.5)
	            			{
	            				piecesBeforeBrake = 2;
	            			}
	            			else if (actualSpeed > 5.5)
	            			{
	            				piecesBeforeBrake = 1;
	            			}
		            		// If the next piece will be a turn, then slowing down now
		            		int nextIndex = 0;
		            		if (currentMyCarPosition.piecePosition.pieceIndex + piecesBeforeBrake >= gameInit.data.race.track.pieces.size())
		            		{
		            			
		            			nextIndex = (currentMyCarPosition.piecePosition.pieceIndex + piecesBeforeBrake) - gameInit.data.race.track.pieces.size();
		            		}
		            		else
		            		{
		            			nextIndex = currentMyCarPosition.piecePosition.pieceIndex + piecesBeforeBrake;
		            		}
		            		double angle = gameInit.data.race.track.pieces.get(nextIndex).angle;
		            		if (Math.abs(gameInit.data.race.track.pieces.get(nextIndex).angle) != 0)
		            		{
		            			if (actualSpeed > 7)
		            			{
		            				carThrottle = 0;
				            		send(new Throttle(carThrottle));
		            			}
		            			else if (actualSpeed > 5.5)
		            			{
			            			carThrottle = 0.2;
				            		send(new Throttle(carThrottle));
		            			}
		            			else
		            			{
		            				carThrottle = 0.7;
				            		send(new Throttle(carThrottle));
		            			}
		            		}
		            		// We are going straight forward for a while, crank it up
		            		else
		            		{
			            		carThrottle = 1;
			            		send(new Throttle(carThrottle));
		            		}
	            		}
	            	}
	            	// There is an angle, slowing down
	            	else
	            	{
		            		carThrottle = 0.6;
		            		send(new Throttle(carThrottle));
	            	}
	            	
	            	debug("Car Position angle: " + currentMyCarPosition.angle + 
	            			", PiecePosition index: " + currentMyCarPosition.piecePosition.pieceIndex + 
	            			", inPieceDistance: " + currentMyCarPosition.piecePosition.inPieceDistance + 
	            			", Lane startindex: " + currentMyCarPosition.piecePosition.lane.startLaneIndex + 
	            			", Lane endindex: " + currentMyCarPosition.piecePosition.lane.endLaneIndex +
	            			", Lap: " + currentMyCarPosition.piecePosition.lap + 
	            			", Speed: " + carThrottle + 
	            			", ActualSpeed: " + actualSpeed);
            	}
            	else
            	{
            		debug("Couldn't find your car!");
            		send(new Ping());
            	}
            } 
            else if (msgFromServer.msgType.equals("join")) 
            {
            	debug("Joined");
            	send(new Ping());
            } 
            else if (msgFromServer.msgType.equals("yourCar"))
            {
            	debug("YourCar received");
            	// Saving the car information for our car in the private variable
            	myCar = gson.fromJson(line, YourCar.class);
            	debug("Message: '" + myCar.msgType + "' Car name: '" + myCar.data.name + "' Color: '" + myCar.data.color + "'");
            	send(new Ping());
            }
            else if (msgFromServer.msgType.equals("gameInit")) 
            {
            	debug("Race init");
            	// Saving information of the track to the private variable gameInit
            	gameInit = gson.fromJson(line, GameInit.class);
            	debug("Track name: " + gameInit.data.race.track.name);
            	for (int i = 0; i < gameInit.data.race.track.pieces.size(); ++i)
            	{
            		debug("Piece " + i + 
            				", Length: " + gameInit.data.race.track.pieces.get(i).length +
            				", Switch: " + Boolean.toString(gameInit.data.race.track.pieces.get(i).svitch) + 
            				", Radius: " + gameInit.data.race.track.pieces.get(i).radius +
            				", Angle: " + gameInit.data.race.track.pieces.get(i).angle);
            	}
            	send(new Ping());
            } 
            else if (msgFromServer.msgType.equals("gameEnd")) 
            {
            	debug("Race end");
            	send(new Ping());
            } 
            else if (msgFromServer.msgType.equals("gameStart")) 
            {
            	debug("Race start, throttle " + carThrottle);
            	send(new Throttle(carThrottle));
            	
            } 
            else if (msgFromServer.msgType.equals("gameEnd"))
            {
            	debug("Game End");
            	GameEnd gameEnd = gson.fromJson(line, GameEnd.class);
            	send(new Ping());
            }
            else if (msgFromServer.msgType.equals("tournamentEnd"))
            {
            	debug("Tournament End");
            	send(new Ping());
            }
            else if (msgFromServer.msgType.equals("crash"))
            {
            	debug("Someone crashed");
            	Crash crash = gson.fromJson(line, Crash.class);
            	send(new Ping());
            }
            else if (msgFromServer.msgType.equals("spawn"))
            {
            	debug("Someone spawned");
            	Spawn spawn = gson.fromJson(line, Spawn.class);
            	send(new Ping());
            }
            else if (msgFromServer.msgType.equals("lapFinished"))
            {
            	debug("Someone finished a lap");
            	LapFinished lapFinished = gson.fromJson(line, LapFinished.class);
            	send(new Ping());
            }
            else if (msgFromServer.msgType.equals("dnf"))
            {
            	debug("Someone was disqualified");
            	Dnf dnf = gson.fromJson(line, Dnf.class);
            	send(new Ping());
            }
            else if (msgFromServer.msgType.equals("finish"))
            {
            	debug("Someone finished the race");
            	Finish finish = gson.fromJson(line, Finish.class);
            	send(new Ping());
            }
            else if (msgFromServer.msgType.equals("turboAvailable"))
            {
            	debug("TURBO AVAILABLE");
            	turbo = gson.fromJson(line, TurboAvailable.class);
            	turboAvailable = true;
            	send(new Ping());
            }
            else 
            {
            	debug("Sending ping");
                send(new Ping());
            }
            
            // Counting the ticks
            ++tick;
        }
        
        debug("Nothing more in reader, ending");
    }
    
    private boolean checkIfCarInTurn()
    {
    	if (gameInit.data.race.track.pieces.get(currentMyCarPosition.piecePosition.pieceIndex).angle != 0)
    	{
    		return true;
    	}
    	
    	return false;
    }
    
    private void debug(String msg)
    {
    	if (debug == true)
    	{
    		System.out.println(msg);
    	}
    }
    
    private void send(final SendMsg msg) 
    {
    	//debug(msg.toJson());
        writer.println(msg.toJson());
        writer.flush();
    }
    
    
    private CarPosition GetCarPosition(CarPositions positions, Car myCar)
    {
    	for (int i = 0; i < positions.data.size(); ++i)
    	{
    		if (positions.data.get(i).id.name.equals(myCar.name) && positions.data.get(i).id.color.equals(myCar.color))
    		{
    			return positions.data.get(i);
    		}
    	}
    	
    	return null;
    }
}

