package UNBot;

import com.google.gson.Gson;

abstract class SendMsg 
{
    public String toJson() 
    {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() 
    {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper 
{
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) 
    {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) 
    {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class Join extends SendMsg 
{
    public final String name;
    public final String key;

    Join(final String name, final String key) 
    {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() 
    {
        return "join";
    }
}

class Ping extends SendMsg 
{
    @Override
    protected String msgType() 
    {
        return "ping";
    }
}

class Throttle extends SendMsg 
{
    private double value;

    public Throttle(double value) 
    {
        this.value = value;
    }

    @Override
    protected Object msgData() 
    {
        return value;
    }

    @Override
    protected String msgType() 
    {
        return "throttle";
    }
}

class Turbo extends SendMsg
{
	private String value;

    public Turbo() 
    {
        this.value = "Pew pew";
    }

    @Override
    protected Object msgData() 
    {
        return value;
    }

    @Override
    protected String msgType() 
    {
        return "turbo";
    }
}