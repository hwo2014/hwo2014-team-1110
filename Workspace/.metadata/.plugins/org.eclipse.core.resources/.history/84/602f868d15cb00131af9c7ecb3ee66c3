package UNBot;

import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

class YourCar
{
	public final String msgType;
	public final Car data;
	
	YourCar(final String msgType, final Car data)
	{
		this.msgType = msgType;
		this.data = data;
	}
}
class Car
{
	public final String name;
	public final String color;
	
	Car(final String name, final String color)
	{
		this.name = name;
		this.color = color;
	}
}

class CarPositions
{
	public final String msgType;
	public final ArrayList<CarPosition>data;
	public final String gameId;
	public final int gameTick;
	
	CarPositions(final String msgType, final ArrayList<CarPosition>data, final String gameId, final int gameTick)
	{
		this.msgType = msgType;
		this.data = data;
		this.gameId = gameId;
		this.gameTick = gameTick;
	}
}
class CarPosition
{
	public final Car id;
	public final double angle;
	public final PiecePosition piecePosition;
	
	CarPosition(final Car id, final double angle, final PiecePosition piecePosition)
	{
		this.id = id;
		this.angle = angle;
		this.piecePosition = piecePosition;
	}
}
class PiecePosition
{
	public final int pieceIndex;
	public final double inPieceDistance;
	public final Lane lane;
	public final int lap;
	
	PiecePosition(final int pieceIndex, final double inPieceDistance, final Lane lane, final int lap)
	{
		this.pieceIndex = pieceIndex;
		this.inPieceDistance = inPieceDistance;
		this.lane = lane;
		this.lap = lap;
	}
}
class Lane
{
	public final int startLaneIndex;
	public final int endLaneIndex;
	
	Lane(final int startLaneIndex, final int endLaneIndex)
	{
		this.startLaneIndex = startLaneIndex;
		this.endLaneIndex = endLaneIndex;
	}
}

class GameInit
{
	public final String msgType;
	public final InitData data;
	
	GameInit(final String msgType, final InitData data)
	{
		this.msgType = msgType;
		this.data = data;
	}
}
class InitData
{
	public final Race race;
	
	InitData(final Race race)
	{
		this.race = race;
	}
}
class Race
{
	public final Track track;
	public final ArrayList<CarInfo>cars;
	public final RaceSession raceSession;
	
	Race(final Track track, final ArrayList<CarInfo>cars, final RaceSession raceSession)
	{
		this.track = track;
		this.cars = cars;
		this.raceSession = raceSession;
	}
}
class Track
{
	public final String id;
	public final String name;
	public final ArrayList<Piece>pieces;
	public final ArrayList<Lanes>lanes;
	public final StartingPoint startingPoint;
	
	Track(final String id, final String name, final ArrayList<Piece>pieces, final ArrayList<Lanes>lanes, final StartingPoint startingPoint)
	{
		this.id = id;
		this.name = name;
		this.pieces = pieces;
		this.lanes = lanes;
		this.startingPoint = startingPoint;
	}
}
class Piece
{
	public final double length;
	@SerializedName("switch")
	public final boolean svitch;
	public final int radius;
	public final double angle;
	
	Piece(final double length, final boolean svitch, final int radius, final double angle)
	{
		this.length = length;
		this.svitch = svitch;
		this.radius = radius;
		this.angle = angle;
	}
}
class Lanes
{
	public final int distanceFromCenter;
	public final int index;
	
	Lanes(final int distanceFromCenter, final int index)
	{
		this.distanceFromCenter = distanceFromCenter;
		this.index = index;
	}
}
class StartingPoint
{
	public final Position position;
	public final int angle;
	
	StartingPoint(final Position position, final int angle)
	{
		this.position = position;
		this.angle = angle;
	}
}
class Position
{
	public final int x;
	public final int y;
	
	Position(final int x, final int y)
	{
		this.x = x;
		this.y = y;
	}
}
class CarInfo
{
	public final Car id;
	public final Dimensions dimensions;
	
	CarInfo(final Car id, final Dimensions dimensions)
	{
		this.id = id;
		this.dimensions = dimensions;
	}
}
class Dimensions
{
	public final double length;
	public final double width;
	public final double guideFlagPosition;
	
	Dimensions(final double length, final double width, final double guideFlagPosition)
	{
		this.length = length;
		this.width = width;
		this.guideFlagPosition = guideFlagPosition;
	}
}
class RaceSession
{
	public final int laps;
	public final int maxLapTimeMs;
	public final boolean quickRace;
	
	public RaceSession(final int laps, final int maxLapTimeMs, final boolean quickRace) 
	{
		this.laps = laps;
		this.maxLapTimeMs = maxLapTimeMs;
		this.quickRace = quickRace;
	}
}

class GameEnd
{
	public final String msgType;
	public final GameEndData data;
	
	GameEnd(final String msgType, final GameEndData data)
	{
		this.msgType = msgType;
		this.data = data;
	}
}
class GameEndData
{
	public final ArrayList<Results>results;
	public final ArrayList<BestLap>bestLaps;
	
	GameEndData(final ArrayList<Results>results, final ArrayList<BestLap>bestLaps)
	{
		this.results = results;
		this.bestLaps = bestLaps;
	}
}
class Results
{
	public final Car car;
	public final Result result;
	
	Results(final Car car, final Result result)
	{
		this.car = car;
		this.result = result;
	}
}
class Result
{
	public final int laps;
	public final int ticks;
	public final int millis;
	
	Result(final int laps, final int ticks, final int millis)
	{
		this.laps = laps;
		this.ticks = ticks;
		this.millis = millis;
	}
}
class BestLap
{
	public final Car car;
	public final Result result;
	
	BestLap(final Car car, final Result result)
	{
		this.car = car;
		this.result = result;
	}
}

class Crash
{
	public final String msgType;
	public final Car data;
	public final String gameId;
	public final int gameTick;
	
	Crash(final String msgType, final Car data, final String gameId, final int gameTick)
	{
		this.msgType = msgType;
		this.data = data;
		this.gameId = gameId;
		this.gameTick = gameTick;
	}
}

class Spawn
{
	public final String msgType;
	public final Car data;
	public final String gameId;
	public final int gameTick;
	
	Spawn(final String msgType, final Car data, final String gameId, final int gameTick)
	{
		this.msgType = msgType;
		this.data = data;
		this.gameId = gameId;
		this.gameTick = gameTick;
	}
}

class LapFinished
{
	public final String msgType;
	public final LapData data;
	public final String gameId;
	public final int gameTick;
	
	LapFinished(final String msgType, final LapData data, final String gameId, final int gameTick)
	{
		this.msgType = msgType;
		this.data = data;
		this.gameId = gameId;
		this.gameTick = gameTick;
	}
}
class LapData
{
	public final Car car;
	public final Result lapTime;
	public final Result raceTime;
	public final Ranking ranking;
	
	LapData(final Car car, final Result lapTime, final Result raceTime, final Ranking ranking)
	{
		this.car = car;
		this.lapTime = lapTime;
		this.raceTime = raceTime;
		this.ranking = ranking;
	}
}
class Ranking
{
	public final int overall;
	public final int fastestLap;
	
	Ranking(final int overall, final int fastestLap)
	{
		this.overall = overall;
		this.fastestLap = fastestLap;
	}
}

// Disqualified car
class Finish
{
	public final String msgType;
	public final Car data;
	public final String gameId;
	public final int gameTick;
	
	Finish(final String msgType, final Car data, final String gameId, final int gameTick)
	{
		this.msgType = msgType;
		this.data = data;
		this.gameId = gameId;
		this.gameTick = gameTick;
	}
}

// Turbo is available
class TurboAvailable
{
	public final String msgType;
	public final TurboData data;
	
	TurboAvailable(final String msgType, final TurboData data)
	{
		this.msgType = msgType;
		this.data = data;
	}
}
class TurboData
{
	public final double turboDurationMilliseconds;
	public final int turboDurationTicks;
	public final double turboFactor;
	
	TurboData(final double turboDurationMilliseconds, final int turboDurationTicks, final double turboFactor)
	{
		this.turboDurationMilliseconds = turboDurationMilliseconds;
		this.turboDurationTicks = turboDurationTicks;
		this.turboFactor = turboFactor;
	}
}